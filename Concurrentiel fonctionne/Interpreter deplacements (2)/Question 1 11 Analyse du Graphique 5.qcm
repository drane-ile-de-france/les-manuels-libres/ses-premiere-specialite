<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 1 Comment un marché concurrentiel fonctionne-t-il&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/01.html"> 
  <meta property="og:description" content="Comment un marché concurrentiel s’équilibre-t-il et comment ce modèle permet-t-il une meilleure compréhension des différents mécanismes de marché&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/chapter-01-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/premiere-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 1.11 Complétez le texte</title>
  <title></title>
 </head> 
 <body class="premiere-ses chapter" data-title="L’Économie pour Première SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-01-11">  
     <p>À l’équilibre initial, représenté par le point A, le prix est de <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="q0ue71el">2</option> <option data-select-code="zs1nb9xt">1</option> <option data-select-code="g43kz7ma">5 000</option> </select></span> euros et <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="26zvuvy1">7 000</option> <option data-select-code="m3xduals">1</option> <option data-select-code="11u59z3z">5 000</option> </select></span> pains sont échangés. En raison d’une modification des conditions de la demande, par exemple <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ww574z1a">une hausse des revenus des consommateurs</option> <option data-select-code="m61rs75e">une baisse des revenus des consommateurs</option> <option data-select-code="gk8tltnx">une baisse des coûts de production de l'entreprise</option> </select></span> (<strong>choc positif</strong>), pour tous niveaux de prix, la courbe de demande se déplace vers <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="g4qk4xlu">la droite</option> <option data-select-code="0kpux4r2">la gauche</option> <option data-select-code="ubbevwkm">le bas</option> </select></span> (<strong>déplacement de la courbe</strong>). À l’ancien prix d’équilibre de <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="o4552lmw">2</option> <option data-select-code="d9fll1ai">1</option> <option data-select-code="jsauff2j">5 000</option> </select></span> euros, <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="zijjhde2">un équilibre</option> <option data-select-code="dpfjaw8m">une surproduction</option> <option data-select-code="bw8m3sf8">une pénurie</option> </select></span> apparaît&nbsp;: la demande est <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="dtrxze4d">égale</option> <option data-select-code="acung4zf">inférieure</option> <option data-select-code="gk1a3qhq">supérieure</option> </select></span> à l’offre. Ce déséquilibre provoque une <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="vc386vo5">hausse</option> <option data-select-code="nlqrdbw4">baisse</option> <option data-select-code="qtnwlwzf">stagnation</option> </select></span> des prix&nbsp;; elle-même conduit à un <strong>déplacement sur les courbes</strong> (La quantité offerte <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="h4t11adt">augmente</option> <option data-select-code="za20dwmt">baisse</option> <option data-select-code="hd5igawx">ne varie pas</option> </select></span> et la quantité demandée <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="8r8aein0">augmente</option> <option data-select-code="xhu65es6">baisse</option> <option data-select-code="ez6ih9hl">ne varie pas</option> </select></span>). Finalement, le nouvel équilibre est représenté par le point B, avec un prix de <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="d1o1464n">2,5</option> <option data-select-code="474lizkg">2</option> <option data-select-code="6vm0eq2o">6 000</option> </select></span> euros et <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="t9roolaw">2,5</option> <option data-select-code="qipxpn1o">5 000</option> <option data-select-code="u1ds54um">6 000</option> </select></span> pains échangés. L’augmentation de la demande a conduit à une <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="xoov4oig">augmentation</option> <option data-select-code="10hnbmtz">baisse</option> <option data-select-code="37w2ulw6">stagnation</option> </select></span> de la quantité et du prix d’équilibre.</p> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>