<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 5 Qu’est-ce que la monnaie et comment est-elle créée&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/text/05.html"> 
  <meta property="og:description" content="En quoi la monnaie sert-t-elle comme un outil pour contrôler l’activité économique et comment la politique monétaire peut-t-elle stimuler ou ralentir une économie&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/premiere-ses/images/web/chapter-05-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/premiere-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 5.1 Complétez le tableau</title>
  <title></title>
 </head> 
 <body class="premiere-ses chapter" data-title="L’Économie pour Première SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-05-01">  
     <p>NB&nbsp;: L’exercice poursuit les comptes précédents de la banque Bonus et du Fournil.</p> 
     <p>Étape 1&nbsp;: M. Dupuis obtient un crédit 1 000 euros de la banque Bonus pour organiser un pique-nique géant de 400 personnes.</p> 
     <p class="table-caption">Extrait du bilan de la banque Bonus après le crédit</p> 
     <table class="even-columns"> 
      <thead> 
       <tr> 
        <th>Actif</th> 
        <th>Passif</th> 
       </tr> 
      </thead> 
      <tbody> 
       <tr> 
        <td>Caisse&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="mx773ngz">500 €</option> <option data-select-code="b5sosaf0">500 + 1 000 = 1 500 €</option> <option data-select-code="imgvlcmh">500 – 1 000 = - 500 €</option> </select></span></td> 
        <td>Compte du Fournil&nbsp;: 500 €</td> 
       </tr> 
       <tr> 
        <td>Crédit accordé à M. Dupuis&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="myh0wzol">0</option> <option data-select-code="ta0d003p">+1 000 €</option> <option data-select-code="k849rdvs">- 1 000 €</option> </select></span></td> 
        <td>Compte de M. Dupuis&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="0sdhkuq6">0</option> <option data-select-code="wyxsk6nj">−1 000 €</option> <option data-select-code="y6jm5fnf">+1 000 €</option> </select></span></td> 
       </tr> 
      </tbody> 
     </table> 
     <p>Étape 2&nbsp;: M. Dupuis commande 400 sandwiches et 400 tartelettes aux pommes à la boulangerie Le Fournil qu’il paie avec un chèque de 1 000 euros.</p> 
     <p class="table-caption">Extrait du bilan de la banque Bonus après l’encaissement du chèque</p> 
     <table class="even-columns"> 
      <thead> 
       <tr> 
        <th>Actif</th> 
        <th>Passif</th> 
       </tr> 
      </thead> 
      <tbody> 
       <tr> 
        <td>Caisse&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ajy93bgi">500 €</option> <option data-select-code="klbhn4d2">500 + 1 000 = 1 500 €</option> <option data-select-code="q3zjkp16">500 – 1 000 = - 500 €</option> </select></span></td> 
        <td>Compte du Fournil&nbsp;: 500 € ÷ 500 + 1 000 = 1&nbsp;500 €</td> 
       </tr> 
       <tr> 
        <td>Crédit accordé M. Dupuis&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="rpptacrq">0 €</option> <option data-select-code="q57e9rqf">1 000 €</option> <option data-select-code="m4ovfyuj">500 – 1 000 = - 500 €</option> </select></span></td> 
        <td>Dette / Compte de M. Dupuis&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ljni3w14">1 000 – 1 000 = 0 €</option> <option data-select-code="1mjdecjt">1 000 €</option> <option data-select-code="dr0fypsg">1 000 + 1 000 = 1 000 €</option> </select></span></td> 
       </tr> 
      </tbody> 
     </table> 
     <p class="table-caption">Extrait du bilan de la boulangerie Le Fournil</p> 
     <table class="even-columns"> 
      <thead> 
       <tr> 
        <th>Actif</th> 
        <th>Passif</th> 
       </tr> 
      </thead> 
      <tbody> 
       <tr> 
        <td>Caisse&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="lcyr6lis">0 €</option> <option data-select-code="o5e9yo5v">+1 000 €</option> <option data-select-code="crkpt5hd">- 1 000 €</option> </select></span></td> 
        <td>&nbsp;</td> 
       </tr> 
       <tr> 
        <td>Compte à la banque Bonus&nbsp;: <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="of0kvgp7">500 €</option> <option data-select-code="h67g7tnc">500 + 1 000 = 1 500 €</option> <option data-select-code="grypnuwo">500 – 1 500 = - 500 €</option> </select></span></td> 
        <td>&nbsp;</td> 
       </tr> 
      </tbody> 
     </table> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>